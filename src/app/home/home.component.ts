import { Component, OnInit } from '@angular/core';
import {BlogPostService} from '../shared/blog-post/blog-post.service';
import {BlogPost} from '../shared/blog-post/blog-post.model';
import {CssStateService} from '../shared/css-state.service';
import {RealisationService} from '../shared/models/realisation.service';
import {Realisation} from '../shared/models/realisation.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  posts: BlogPost[];
  realisations: Realisation[];
  // tslint:disable-next-line:max-line-length
  constructor(private blogPostService: BlogPostService, private bodyClassService: CssStateService, private realisationService: RealisationService) { }

  ngOnInit(): void {
    this.bodyClassService.setCssState('home');
    this.blogPostService.findAll()
      .subscribe(res => this.posts = res.body);

    this.realisationService.findAll()
      .subscribe(reaResponse => this.realisations = reaResponse.body );
  }

}
