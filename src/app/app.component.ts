import {Component, OnInit} from '@angular/core';
import {CssStateService} from './shared/css-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Bes Webdev';

  bodyClass = '';
  // fruits = [];


  constructor(private bodyClassService: CssStateService) {
  }

  ngOnInit(): void {
    this.bodyClassService.state.subscribe(value => this.bodyClass = value);
  }

  reset(): void {

  }

}
