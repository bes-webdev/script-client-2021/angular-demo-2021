import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContactMessage} from '../shared/models/contact-message.model';
import {ContactService} from '../shared/contact/contact.service';
import {fromPromise} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;

  @ViewChild('myImage') myImage: ElementRef;

  constructor(private formBuilder: FormBuilder, private contactService: ContactService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.contactForm = this.formBuilder.group({
      email: ['', Validators.email],
      name: ['', Validators.required],
      message: ['', Validators.required],
      image: ['']
    });
  }

  onSubmit(): void {
    if (this.contactForm.valid) {
      const contactMessage = new ContactMessage(
        this.contactForm.value.email,
        this.contactForm.value.name,
        this.contactForm.value.message
      );
      const reader = new FileReader();
      reader.readAsDataURL(this.myImage.nativeElement.files[0]);
      reader.onload  =  () => {
        contactMessage.image = reader.result as string;
        contactMessage.imageName = this.contactForm.value.image;
        this.contactService.create(contactMessage)
          .subscribe();
        // Envoyer le message vers le server avec un service
        console.log('nouveau message', contactMessage);
      };


    }
  }

}
