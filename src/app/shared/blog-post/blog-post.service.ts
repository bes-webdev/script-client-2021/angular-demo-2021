import { Injectable } from '@angular/core';
import {BaseApi} from '../blog-api/base-api';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BlogPost} from './blog-post.model';

@Injectable({
  providedIn: 'root'
})
export class BlogPostService extends BaseApi {

  constructor(private httpClient: HttpClient) {
    super();
  }

  findAll(searchParams?: any): Observable<HttpResponse<BlogPost[]>> {
    return this.httpClient.get<BlogPost[]>(this.SERVER_URL + 'blog_posts', {params: searchParams, headers: this.httpHeaders, observe: 'response'});
  }

  find(id: number): Observable<HttpResponse<BlogPost>> {
    return this.httpClient.get<BlogPost>(this.SERVER_URL + 'blog_posts/' + id, {headers: this.httpHeaders, observe: 'response'});
  }
}
