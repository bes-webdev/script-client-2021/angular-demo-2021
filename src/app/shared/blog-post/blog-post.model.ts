export class BlogPost {

  constructor(public id?: number, public title?: string, public body?: string, public likes?: number, public category?: number) {
  }
}
