import {Directive, ElementRef} from '@angular/core';
import {CssStateService} from '../css-state.service';

@Directive({
  selector: '[appBodyClass]'
})
export class BodyClassDirective {

  constructor(el: ElementRef, private bodyClassService: CssStateService) {
    this.bodyClassService.state.subscribe(val => el.nativeElement.className = val);
  }


}
