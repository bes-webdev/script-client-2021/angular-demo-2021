import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Category} from './categorie.model';
import {BaseApi} from '../blog-api/base-api';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends BaseApi {

  constructor(private http: HttpClient) {
    super();
  }

  findAll(): Observable<HttpResponse<Category[]>> {
    return this.http.get<Category[]>(this.SERVER_URL + 'categories', {headers: this.httpHeaders, observe: 'response'});
  }

  find(id: number): Observable<HttpResponse<Category>> {
    return this.http.get<Category>(this.SERVER_URL + 'categories/' + id, {headers: this.httpHeaders, observe: 'response'});
  }

  // Exemple pour quand on n'a pas encore d'api et qu'on veut avancer sur les services
  findBeforeHavingAnApi(id: number): Observable<HttpResponse<Category>> {
    const category = new Category(1, 'hard coded category');
    // of() erst une function de RXJs qui fabrique un observable sur base de l'argument donné
    return of(new HttpResponse({body: category} ));
  }

}
