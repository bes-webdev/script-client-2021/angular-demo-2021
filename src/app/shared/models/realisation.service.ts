import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {Realisation} from './realisation.model';

@Injectable({
  providedIn: 'root'
})
export class RealisationService {

  constructor() { }

  findAll(): Observable<HttpResponse<Realisation[]>> {
    const realisations = [
      new Realisation(1, 'première rea', 'http://localhost/image.png'),
      new Realisation(2, 'deuxième rea', 'http://localhost/image.png')
    ];

    return of(new HttpResponse({body: realisations}));
  }
}
