import { DateTime } from 'luxon';
import {BlogPost} from '../blog-post/blog-post.model';

export class Comment {
  constructor(public id?: number, public content?: string, public user?: string, public date?: DateTime, public post?: string) {
  }
}
