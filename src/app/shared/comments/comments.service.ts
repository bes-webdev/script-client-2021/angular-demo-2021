import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseApi} from '../blog-api/base-api';
import {Comment} from './comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentsService extends BaseApi {

  constructor(private httpClient: HttpClient) { super(); }

  findAll(searchParams: any): Observable<HttpResponse<Comment[]>> {
    return this.httpClient.get<Comment[]>(this.SERVER_URL + 'comments', {params: searchParams, headers: this.httpHeaders, observe: 'response'});
  }

  create(comment: Comment): Observable<HttpResponse<Comment>> {
    return this.httpClient.post<Comment>(this.SERVER_URL + 'comments', this.postIdToIri(comment), {headers: this.httpHeaders, observe: 'response'});
  }

  postIdToIri(comment: Comment): Comment {
    comment.post = '/blog/back/api/blog_posts/' + comment.post;
    return comment;
  }
}
