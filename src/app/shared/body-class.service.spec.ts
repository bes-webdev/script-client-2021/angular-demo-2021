import { TestBed } from '@angular/core/testing';

import { CssStateService } from './css-state.service';

describe('BodyClassService', () => {
  let service: CssStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CssStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
