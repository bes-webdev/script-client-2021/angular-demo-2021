import {HttpHeaders} from '@angular/common/http';

export abstract class BaseApi { // abstract veut dire que la classe est destinée à etre héritée, on ne peut pas l'instancier.

  private readonly _SERVER_URL = 'https://prof2.bes-webdeveloper-seraing.be/blog/back/api/';

  // tslint:disable-next-line:variable-name
  private _httpHeaders = new HttpHeaders({
    'Content-Type':  'application/json',
    Accept: 'application/json'
  });

  get SERVER_URL(): string {
    return this._SERVER_URL;
  }


  get httpHeaders(): HttpHeaders {
    return this._httpHeaders;
  }
}
