import { Component, OnInit } from '@angular/core';
import {BlogPostService} from '../shared/blog-post/blog-post.service';
import {ActivatedRoute} from '@angular/router';
import {BlogPost} from '../shared/blog-post/blog-post.model';
import {HttpResponse} from '@angular/common/http';
import {Comment} from '../shared/comments/comment.model';
import {CommentsService} from '../shared/comments/comments.service';
import {CssStateService} from '../shared/css-state.service';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {

  post: BlogPost;
  comments: Comment[];
  newComment: Comment;

  // tslint:disable-next-line:max-line-length
  constructor(private activatedRoute: ActivatedRoute, private commentsService: CommentsService, private bodyClassService: CssStateService) { }

  ngOnInit(): void {
    this.bodyClassService.setCssState('post');
    this.newComment =  new Comment();
    this.activatedRoute.data
      .subscribe((data: { post: HttpResponse<BlogPost> }) => {
        this.post = data.post.body;
        this.resetNewComment();
        this.fetchComments();
      });
  }

  fetchComments(): void {
    this.commentsService.findAll({'post.id': this.post.id})
      .subscribe(res => this.comments = res.body);
  }

  createComment(): void {
    if (this.newComment.content && this.newComment.user) {
      this.commentsService.create(this.newComment)
        .subscribe(res => {
          this.fetchComments();
          this.resetNewComment();
        });
    }
  }

  resetNewComment(): void {
    this.newComment =  new Comment();
    this.newComment.post = '' + this.post.id;
  }
}
