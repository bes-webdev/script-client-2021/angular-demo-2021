import { Component, OnInit } from '@angular/core';
import {Category} from '../shared/categories/categorie.model';
import {ActivatedRoute, Router} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {CategoriesService} from '../shared/categories/categories.service';
import {BlogPostService} from '../shared/blog-post/blog-post.service';
import {BlogPost} from '../shared/blog-post/blog-post.model';
import {faThumbsUp} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  category: Category;
  posts: BlogPost[];

  constructor(
    private categoryService: CategoriesService,
    private activatedRoute: ActivatedRoute,
    private blogPostService: BlogPostService ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        // Chaque mergeMap prend un Observable en argument et en retourne un autre.
        mergeMap(params => this.categoryService.find(params.id)),
        mergeMap(categoryRes => {
          this.category = categoryRes.body;
          return this.blogPostService.findAll({'category.id': this.category.id});
        })
      )
      .subscribe(postResponse => this.posts = postResponse.body );
    // Sans le pipe() ça donnerait ceci
    // this.activatedRoute.params
    //   .subscribe(params => {
    //     this.categoryService.find(params.id)
    //       .subscribe( res => {
    //         this.category = res.body;
    //         this.blogPostService.findAll({'category.id': this.category.id})
    //           .subscribe(res2 => this.posts = res2.body);
    //       });
    //   });
  }

}
