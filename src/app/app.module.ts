import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MyNavComponent} from './my-nav/my-nav.component';
import { HomeComponent } from './home/home.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { ContactComponent } from './contact/contact.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoryComponent } from './category/category.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BlogPostPreviewComponent } from './blog-post-preview/blog-post-preview.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BodyClassDirective } from './shared/directives/body-class.directive';

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    HomeComponent,
    BlogPostComponent,
    ContactComponent,
    CategoryComponent,
    BlogPostPreviewComponent,
    BodyClassDirective
  ],
  exports: [BodyClassDirective],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
