import {Component, Input, OnInit} from '@angular/core';
import {BlogPost} from '../shared/blog-post/blog-post.model';
import {faThumbsUp} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-blog-post-preview',
  templateUrl: './blog-post-preview.component.html',
  styleUrls: ['./blog-post-preview.component.css']
})
export class BlogPostPreviewComponent implements OnInit {

  @Input()
  post: BlogPost;

  faThumb = faThumbsUp;

  constructor() { }

  ngOnInit(): void {
  }

}
