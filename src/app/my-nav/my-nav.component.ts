import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../shared/categories/categorie.model';
import {CategoriesService} from '../shared/categories/categories.service';
import {CssStateService} from '../shared/css-state.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent implements OnInit {
  isAuth = false;
  categories: Category[] = [];
  cssState = '';
  constructor(private categoriesService: CategoriesService, private cssStateService: CssStateService) { }

  ngOnInit(): void {
    this.cssStateService.state.subscribe(state => this.cssState = state);
    this.categoriesService.findAll()
      .subscribe(response => this.categories = response.body);
  }

  logIn(): void {
    this.isAuth = true;
  }

  logOff(): void {
    this.isAuth = false;
  }
}
