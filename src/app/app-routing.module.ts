import {Injectable, NgModule} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {BlogPostComponent} from './blog-post/blog-post.component';
import {ContactComponent} from './contact/contact.component';
import {CategoryComponent} from './category/category.component';
import {BlogPost} from './shared/blog-post/blog-post.model';
import {BlogPostService} from './shared/blog-post/blog-post.service';
import {Observable} from 'rxjs';


@Injectable( {
  providedIn: 'root'
})
export class BlogPostResolver implements Resolve<BlogPost> {
  constructor(private service: BlogPostService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    return this.service.find(parseInt(route.paramMap.get('id'), 10));
  }
}

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'post/:id',
    component: BlogPostComponent,
    resolve: {
      post: BlogPostResolver
    }
  },
  {
    path: 'category/:id',
    component: CategoryComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
